//
//  GetWetherProtocal.swift
//  Clima
//
//  Created by Jeerapat Sripumngoen on 24/6/2563 BE.
//  Copyright © 2563 App Brewery. All rights reserved.
//

import Foundation

protocol GetWeatherProtocal {
    func getData(weather: WeatherModel)
}
