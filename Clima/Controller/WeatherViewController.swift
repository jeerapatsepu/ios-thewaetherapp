//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchTextfield: UITextField!

    var wetherManager = WeatherManager()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager.requestWhenInUseAuthorization()
        
        
        searchTextfield.delegate = self
        wetherManager.delegate = self
        locationManager.delegate = self
    }

    @IBAction func searchButtonAction(_ sender: UIButton) {
        searchTextfield.endEditing(true)
    }
    @IBAction func currentGPSAction(_ sender: UIButton) {
        locationManager.requestLocation()
    }
    
}

extension WeatherViewController: GetWeatherProtocal {
    func getData(weather: WeatherModel) {
    //        print(weather.icon)
            
        DispatchQueue.main.async {
            self.temperatureLabel.text = String(weather.temp)
            self.cityLabel.text = weather.city
            
            self.setIcon(uri: weather.icon)
        }
    }
    
    func setIcon(uri: String) {
            guard let imageURL = URL(string: uri) else { return }

                // just not to cause a deadlock in UI!
    //        DispatchQueue.global().async {
                guard let imageData = try? Data(contentsOf: imageURL) else { return }

                let image = UIImage(data: imageData)
    //            DispatchQueue.main.async {
                    self.conditionImageView.image = image
    //            }
    //        }
        }
}

extension WeatherViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing!")
        print(textField.text!)
        wetherManager.getCity(city: textField.text!)

        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn! -> true")
        print(textField.text!)
        wetherManager.getCity(city: textField.text!)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text! != "" {
            print("textFieldShouldEndEditing! -> true")
            print(textField.text!)
            return true
        }
        else {
            print("textFieldShouldEndEditing! -> false")
            textField.placeholder = "wanna search for?"
            return false
        }
    }

}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            
//            print(Double(lat))
            
            wetherManager.getCity(lat: lat, lon: lon)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }

    }
}
