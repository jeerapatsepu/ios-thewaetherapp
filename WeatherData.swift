//
//  Wether.swift
//  Clima
//
//  Created by Jeerapat Sripumngoen on 24/6/2563 BE.
//  Copyright © 2563 App Brewery. All rights reserved.
//

import Foundation

struct WeatherData: Decodable {
    let timezone: Int
    let main: Main
    let name: String
    let weather: [Weather]
}

struct Main: Decodable {
    let temp: Double
}

struct Weather: Decodable {
    let icon: String
}
