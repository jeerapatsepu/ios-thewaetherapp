//
//  ObjcDisplay.swift
//  Clima
//
//  Created by Jeerapat Sripumngoen on 24/6/2563 BE.
//  Copyright © 2563 App Brewery. All rights reserved.
//

import Foundation


class WeatherModel {
    let icon: String
    let temp: Double
    let city: String
    
    init(icon: String, temp: Double, city: String) {
        self.icon = icon
        self.temp = temp
        self.city = city
    }
}
