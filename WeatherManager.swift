//
//  Wether.swift
//  Clima
//
//  Created by Jeerapat Sripumngoen on 24/6/2563 BE.
//  Copyright © 2563 App Brewery. All rights reserved.
//

import Foundation

struct WeatherManager {
    
    let baseUri = "https://api.openweathermap.org/data/2.5/weather"
    
    var delegate: GetWeatherProtocal?
    
    func getCity(city: String) {
        let uri = "\(baseUri)?q=\(city)&appid=5250219e4bfdb0fe56e714c44688d03c&units=metric"
        print(uri)
        connection(uri: uri)
    }
    
    func getCity(lat: Double, lon: Double) {
        let uri = "\(baseUri)?lat=\(lat)&lon=\(lon)&appid=5250219e4bfdb0fe56e714c44688d03c&units=metric"
        print(uri)
        connection(uri: uri)
    }
    
    func connection(uri: String) {
        if let connectionUri = URL(string: uri) {
            
            let uriSession = URLSession(configuration: .default)
            
            let sessionTask = uriSession.dataTask(with: connectionUri) { (data, uriResponse, error) in
                
                if data != nil {
//                    print(String(data: data!, encoding: .utf8)!)
                    self.parseJson(data: data!)
                }
                
                if error != nil {
                    
                }
                
            }
            
            sessionTask.resume()
        }
    }
    
    func parseJson(data: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(WeatherData.self, from: data)
            print(decoded.main.temp)
            
            let temp = decoded.main.temp
            let city = decoded.name
            let icon = "https://openweathermap.org/img/wn/\(decoded.weather[0].icon)@2x.png"

            let weatherModel = WeatherModel(icon: icon, temp: temp, city: city)
            
            delegate?.getData(weather: weatherModel)
            
            return weatherModel
            
        } catch {
            print(error)
            return nil
        }
        
    }
}
